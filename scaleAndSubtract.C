// Code to grab PA and livetime plots and scale the PA to be a rate
TH1D* ScalePAByLivetime(TFile* f) {

  TH1D* h_PA = (TH1D*)f->Get("PA");
  TH1D* h_N = (TH1D*)f->Get("nEvents");

  double nEvents = h_N->GetBinContent(2);
  double livetime = nEvents*4.E-3;
  //4E-3 for Na22, Co57, and Th228
  
  h_PA->SetStats(0);
  h_PA->Sumw2();
  h_PA->SetTitle("; pulse area [phd]; Rate [Hz]");
  h_PA->Scale(1/livetime);

  return h_PA;
}
// Code to subtract a background spectrum from a source spectrum
TCanvas* BkgSubtractAndDraw(TH1D* h_bkg, TH1D* h_source) {

  h_source->Add(h_bkg, -1);
  h_source->SetTitle("Background subtracted source spectrum");
  TCanvas* c = new TCanvas();
  c->cd();
  h_source->Draw();

  return c;
}
// MAIN
void scaleAndSubtract() {

  TFile* f_eBkg = new TFile("~/DATA/AM_bkg_moreBins.root", "READ");
  TH1D* h_eBkg = ScalePAByLivetime(f_eBkg);

  /*  TCanvas* c2 = new TCanvas();
  c2->cd();
  h_eBkg->SetTitle("Background");
  h_eBkg->Draw("HIST");*/

  /*
  TFile* outfile = new TFile("scaled.root", "RECREATE");
  outfile->cd();
  h_eBkg->Write();
  outfile->Write();
  outfile->Close();*/
  
  TFile* f_Na22 = new TFile("~/DATA/AM_sourcePA_Th228.root");
  TH1D* h_Na22 = ScalePAByLivetime(f_Na22);
  
  /*  TCanvas* c3 = new TCanvas();
  c3->cd();
  h_Na22->SetTitle("Source");
  h_Na22->Draw("HIST");*/
  
  TCanvas* c1 = BkgSubtractAndDraw(h_eBkg, h_Na22);
  c1->Draw();
  std::cout << "Source rate above 0 keV: " << h_Na22->Integral(0., 1200.) << " Hz" << std::endl;
  std::cout << "and above 200 keV: " << h_Na22->Integral(56., 1200.) << " Hz" << std::endl;
  std::cout << "and above 2 phd: " << h_Na22->Integral(2, 1200) << std::endl;

}
