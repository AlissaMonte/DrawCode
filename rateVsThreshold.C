void rateVsThreshold() {
  TFile* f = new TFile("~/DATA/skinWS_earlyWS.root", "READ");
  TH1F* hPA = (TH1F*)f->Get("skinPA");
  TH1F* hPA5 = (TH1F*)f->Get("skinPA5");
  TH1F* hC = (TH1F*)f->Get("skinC");
  hPA->Draw("HIST");

  const int n = 500;
  float PA[n];
  float ratePA[n];
  float exPA[n];
  float eyPA[n];
  float ratePA5[n];
  float eyPA5[n];
  const int nC = 131;
  float C[n];
  float rateC[n];
  float exC[n];
  float eyC[n];
  int count = 0;
  //  float binWidthScaleFactor = m_hists->GetHistFromMap("A-LookHereFirst/PulseAreaRand")->GetBinWidth(0) * 2.; 
  for (int i=0; i<n; i+=1) {
    PA[count] = i;
    ratePA[count] = hPA->Integral(hPA->FindBin(i),-1); // /binWidthScaleFactor
    ratePA5[count] = hPA5->Integral(hPA5->FindBin(i),-1); 
    exPA[count] = hPA->GetBinWidth(i)/2.;
    eyPA[count] = sqrt(hPA->GetBinContent(hPA->FindBin(i)));
    eyPA5[count] = sqrt(hPA5->GetBinContent(hPA5->FindBin(i)));
    count++;
  }
  count = 0;
  for(int i = 0; i < nC; i++) {
    C[count] = i;
    rateC[count] = hC->Integral(hC->FindBin(i),-1);
    exC[count] = hC->GetBinWidth(i)/2.;
    eyC[count] = 0.;//sqrt(hC->GetBinContent(hC->FindBin(i)));
    count++;
  }

  TCanvas* c1 = new TCanvas();
  c1->cd();
  TGraphErrors *grPA = new TGraphErrors(n, PA, ratePA, exPA, eyPA);
  grPA->SetTitle("Rate vs threshold for coinc > 2 pulses");
  grPA->GetXaxis()->SetTitle("Skin Threshold (phe)");
  grPA->GetYaxis()->SetTitle("Skin Background Rate (Hz)");
  grPA->Draw("ALP");

  TCanvas* c2 = new TCanvas();
  c2->cd();
  TGraphErrors *grPA5 = new TGraphErrors(n, PA, ratePA5, exPA, eyPA5);
  grPA5->SetTitle("Rate vs threshold for coinc > 5 pulses");
  grPA5->GetXaxis()->SetTitle("Skin Threshold (phe)");
  grPA5->GetYaxis()->SetTitle("Skin Background Rate (Hz)");
  grPA5->Draw("ALP");

  TCanvas* c3 = new TCanvas();
  c3->cd();
  TGraphErrors *grC = new TGraphErrors(nC, C, rateC, exC, eyC);
  grC->SetTitle("Rate vs coinc for coinc > 2 pulses");
  grC->GetXaxis()->SetTitle("Skin Coinc Threshold");
  grC->GetYaxis()->SetTitle("Skin Background Rate (Hz)");
  grC->Draw("ALP");

  TFile* outfile = new TFile("output/earlyWS_skinRates.root", "RECREATE");
  outfile->cd();
  grPA->Write("gPA");
  grPA5->Write("gPA5");
  grC->Write("gC");

  outfile->Write();
  outfile->Close();
}
