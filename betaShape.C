class BetaShape{
  public:
    double operator() (double *x,double *p){
       double p0  =     0.177237;
       double p1  =    0.0305922;
       double p2  =    0.0053915;
       double p3  =   -0.0740132;
       double p4  =    0.0258067;


       //Nominal
       double density_of_states;
       double shape_factor;
       double fermi;
       double phePerkeV = p[1];
       //double phePerkeV = p[1] * ((p0 + p1*log(x[0]) + p2*log(x[0])*log(x[0]))/(1 + p3*log(x[0]) + p4*log(x[0])*log(x[0])));

       double shapeFactor = p[0]*0.511; //so input value is in MeV^(-1)
       const double T0 = (156./511.); //endpoint
       const double me = 1; //electron mass
       double T = x[0]/511./phePerkeV; //x[0] in phe
       
       density_of_states = TMath::Sqrt(T*T+2*T)*(T0-T)*(T0-T)*(T+1);
       shape_factor = 1 + shapeFactor*(T+1);
       fermi = 1.16958 + 0.103086*pow(T*T+2*T,-1.3427/2.0);

       double bkgExpo1 = p[2]*exp(-x[0]/p[3]);
       double bkgExpo2 = p[8]*exp(-x[0]/p[9]);
       double GdGaus = p[4]*exp(-(x[0]-p[5])*(x[0]-p[5])/(2*p[6]*p[6]));
       double beta = density_of_states * shape_factor * fermi;
       double bkg = bkgExpo1 + bkgExpo2 + GdGaus + p[7];
       if(T<T0) return bkg+beta; //density_of_states * shape_factor * fermi
       else return bkg; //0
    }
};

double gaus(double* x, double* params) {
  double A = params[0];
  double mean = params[1];
  double sigma = params[2];
  double t = x[0];

  return (A*exp(-(t-mean)*(t-mean)/(2*sigma*sigma)));
}

double expo(double* x, double* params) {
  double A = params[0];
  double tau = params[1];
  double t = x[0];

  return (A*exp(-t/tau));
}

Double_t SkewGaus(double x, double mean, double sigma){
   // return value of skew gaussian
  if(sigma == 0) return 1.e30;
   double a = 1.22108;
   double b = 4.64365;
   double c = 3.68478;
   double d = 2.84769e-4;

   double skew = sqrt( a*a + (b*b)/mean + (c*c)/(mean*mean) ) + d*mean + 0.15;
   double delta = skew / sqrt(1+skew*skew);
   double loc = mean - (sqrt(2)*delta*sigma)/sqrt(-2*delta*delta+TMath::Pi());
   double width = (sqrt(TMath::Pi())*sigma)/sqrt(-2*delta*delta+TMath::Pi());

   double arg = (x-loc)/width;
   double res = (1./(width*sqrt(2.*TMath::Pi())))*exp(-0.5*arg*arg)*TMath::Erfc( -skew*(1./sqrt(2))*arg );

   return res;
}

TH1F* SmearHistNewReflect( TH1F* h, double e_scale, double res_scale ){

   TH1F* h3 = (TH1F*)h->Clone();
   h3->Reset();
   //make new histogram with negative bins                                                                                           
   const UInt_t nX = h->GetNbinsX();
   double max = h->GetXaxis()->GetXmax();
   TH1F* h2 = new TH1F("h2","",2*nX,-max,max);
   double newcontent, content;
   double x, w;
   Double_t sX = res_scale;
   Double_t sW = res_scale;
   UInt_t kmin, kmax;
   for(UInt_t i=1;i<=nX;++i){
      x = h->GetXaxis()->GetBinCenter(i);
      content = h->GetBinContent(i);
      sX = x*sqrt( 0.0865*0.0865 + (res_scale*res_scale)/x );
      kmin = 1;
      kmax = 2*nX;
      for(UInt_t k=kmin;k<=kmax;++k){
         w = h2->GetXaxis()->GetBinCenter(k);
	 newcontent = h2->GetBinContent(k) + content*SkewGaus(w,x,sX)*h2->GetBinWidth(k);
         h2->SetBinContent(k,newcontent);
      }
   }
   for(UInt_t i=nX+1;i<=2*nX;++i){
      h3->SetBinContent( i-nX, h2->GetBinContent(i) );
   }
   for(UInt_t i=1;i<=nX;++i){
      h3->SetBinContent( nX-(i-1), h3->GetBinContent(nX-(i-1)) + h2->GetBinContent(i) );
   }
   delete h2;
   return h3;
}
   
void betaShape() {

  ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(10000);
  TFile* f = new TFile("~/DATA/scaledHighStatsZ.root"); //scaled.root
  TH1D* h = (TH1D*)f->Get("PAZ");
  h->GetXaxis()->SetRangeUser(0, 60);
  h->SetTitle("Fit to OD background spectrum from Sept 6th");
  
  TF1* f_exp = new TF1("f_exp", "expo", 0, 200, 2);
  f_exp->SetParameters(10000, 30);
  h->Fit("f_exp", "R", "", 6.5, 11);
  
   BetaShape fbeta;
   TF1* bf = new TF1("bf", fbeta, 0, 200, 10);
   bf->SetParNames("ABeta", "EScale", "AExpo", "tau", "AGaus", "mean", "sigma", "const", "AExpo2", "tau2");
   //   bf->SetParameters(100, 0.256, 2.14333e+02, 2.81633, 10, 17, 4, 0.14, 20, 200);
   //   bf->SetParameters(100, 0.237, 778.8, 1.8, 2.15, 18.7, 4.88, 0.14, 1.33, 17.9);
   bf->SetParameters(100, 0.237, 25.9, 3, 2.15, 19, 4.35, 0.1, 1.33, 17.9);
   //bf->SetParLimits(0, 100, 100000);
   //bf->SetParLimits(1, 0.22, 0.3);
   bf->SetParLimits(2, 0, 10000);
   //   bf->SetParLimits(2, 100, 900);
   //   bf->SetParLimits(3, 1, 5);
   //bf->SetParLimits(4, 0, 1000);
   //bf->SetParLimits(7, 0, 100000);
   //   bf->SetParLimits(5, 10, 25);
   bf->SetParLimits(6, 2, 5);
   bf->SetParLimits(8, 0, 10000);
   bf->SetLineColor(kBlack);   
   //   h->Draw("HIST");
   //   bf->Draw();
   
   h->Fit("bf", "R", "", 7, 60);

   //Break out the individual bits
   TF1* betaOnly = new TF1("betaOnly", fbeta, 0, 200, 10);
   betaOnly->SetParameters(bf->GetParameter(0), bf->GetParameter(1), 0, 0, 0, 0, 0, 0, 0, 0);
   betaOnly->SetLineColor(TColor::GetColor("#EF476F")); //#EF476F
   betaOnly->SetLineStyle(2);
   betaOnly->Draw("SAME");
   std::cout << "Integrated beta shape C14 rate: " << betaOnly->Integral(0, 200) << " Hz." << std::endl;
   TF1* f_gaus2 = new TF1("gaus", "gaus", 0, 200, 3);
   f_gaus2->SetParameters(bf->GetParameter(4), bf->GetParameter(5), bf->GetParameter(6));
   f_gaus2->SetLineColor(TColor::GetColor("#540D6E")); //#540D6E
   f_gaus2->SetLineStyle(2);
   f_gaus2->Draw("SAME");
   std::cout << "Integrated gaussian Gd152 rate: " << f_gaus2->Integral(0, 200) << " Hz." << std::endl;
   TF1* f_expo2 = new TF1("expo", "expo", 0, 200, 2);
   f_expo2->SetParameters(bf->GetParameter(2), bf->GetParameter(3));
   f_expo2->SetLineColor(TColor::GetColor("#EF8A17")); //#EF8A17
   f_expo2->SetLineStyle(2);
   f_expo2->Draw("SAME");
   TF1* f_expo3 = new TF1("expo", "expo", 0, 200, 2);
   f_expo3->SetParameters(bf->GetParameter(8), bf->GetParameter(9));
   f_expo3->SetLineColor(TColor::GetColor("#C6C013")); //#C6C013
   f_expo3->SetLineStyle(2);
   f_expo3->Draw("SAME");
   TLine* l = new TLine(0, bf->GetParameter(7), 60, bf->GetParameter(7));
   l->SetLineStyle(2);
   l->SetLineColor(TColor::GetColor("#06D6A0")); //#06D6A0
   l->Draw("SAME");

   TLegend* leg = new TLegend(0.6, 0.55, 0.875, 0.875);
   leg->AddEntry(h, "Data", "pe");
   leg->AddEntry(bf, "Full Fit", "l");
   leg->AddEntry(f_gaus2, "Gd152 alpha", "l");
   leg->AddEntry(betaOnly, "C14 beta", "l");
   leg->AddEntry(f_expo2, "fast bkg expo", "l");
   leg->AddEntry(f_expo3, "slow bkg expo", "l");
   leg->AddEntry(l, "const", "l");
   leg->Draw("SAME");
}
