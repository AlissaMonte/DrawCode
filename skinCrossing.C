#define PI 3.14159265359

void skinCrossing() {

  double chID[93];
  double chTheta[93];
  int j = 0;
  for(int i = 0; i < 93; i++) {
    if(i == 15 || i == 46 || i == 77) j++;
    chID[i] = i+600;
    chTheta[i] = j*3.75*(PI/180.); //degrees
    j++;
  }
  //These came from me looking at ALPACA output and judging cross time by eye
  //I am not a perfect being so take with a grain of salt
  //Some channels were acting up and didn't see a crossing time, I give them the same value
  //as the preceeding channel with .53 on the end, so I know where they were
  double crossT[93] = {9910., 10083., 9910., 8999., 8587., 8478., 8630., 8479., 7871., 7763.,
                       6700., 6683., 6010., 6010., 6364., 5585., 5407., 4982., 5000., 4467.,
		       4288., 4557., 4557.53, 4020., 3930., 4422., 3706., 3482., 3169., 3438.,
		       3214., 2900., 2900., 2900., 2677., 2363., 2363., 2587., 3035., 2766.,
		       2811., 2721., 2005., 2095., 2005., 2318., 796., 1648., 2408., 3259.,
		       3124., 3393., 4154., 4781., 4736., 5273., 5676., 4960., 5363., 5542.,
		       5989., 5989., 5989.53, 7243., 7243., 7870., 7870., 7870., 8138., 9123., 9034.,
		       9392., 9571., 9213., 9078., 8989., 9123., 9123.53, 10914., 10928., 10683.,
		       10333., 10263, 10333., 10566., 9855., 10100., 10275., 10275., 9913., 9680.,
		       10100., 9983.};

  TCanvas* c1 = new TCanvas();
  c1->cd();
  TGraph* gID = new TGraph(93, chID, crossT);
  gID->SetTitle("Liquid crossing skin PMT faces; chID; cross time relative to start of data [s]");
  gID->SetMarkerStyle(3);
  gID->SetLineColor(kBlue);
  gID->Draw("ALP");

  TCanvas* c2 = new TCanvas();
  c2->cd();
  TGraph* gTheta = new TGraph(93, chTheta, crossT);
  gTheta->SetTitle("Liquid crossing skin PMT faces; chTheta [rad]; cross time relative to start of data [s]");
  gTheta->SetMarkerStyle(3);
  gTheta->SetLineColor(kViolet);
  gTheta->Draw("AP");

  TF1 *f1 = new TF1("f1","[0]*cos([1]*x-[2])+[3]", 0, 2*PI);
  f1->SetParameters(3000, 1, 0, 5000);
  f1->FixParameter(1, 1.);
  f1->SetParNames("Amp", "period", "shift", "const");
  gTheta->Fit("f1", "L");
}
